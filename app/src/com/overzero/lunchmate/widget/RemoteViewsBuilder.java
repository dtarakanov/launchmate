package com.overzero.lunchmate.widget;

import android.app.PendingIntent;
import android.content.Context;
import android.view.View;
import android.widget.RemoteViews;

import com.overzero.lunchmate.util.FormatUtils;
import com.overzero.lunchmate.R;
import com.overzero.lunchmate.record.Record;

import java.util.List;

public class RemoteViewsBuilder {

    public RemoteViewsBuilder(Context context) {
        context_ = context;

        remoteViews_ = createView(R.layout.lm_app_widget);
    }

    public void setRecords(List<Record> records) {
        if (records.isEmpty()) {
            setText(R.id.lm_app_widget_text,
                    context_.getString(
                            R.string.lm_help_text));
        } else {
            setText(R.id.lm_app_widget_text,
                    FormatUtils.format(
                            records,
                            context_.getString(
                                    R.string.lm_widget_balance_row_format)));
        }
    }

    public void setInProgress(boolean inProgress) {
        setVisibility(
                R.id.lm_app_widget_progress,
                inProgress ? View.VISIBLE : View.GONE);
    }

    public void setOnListClickIntent(PendingIntent intent) {
        setOnClickIntent(R.id.lm_app_widget_text, intent);
    }

    public void setOnEditClickIntent(PendingIntent intent) {
        setOnClickIntent(R.id.lm_app_widget_edit_button, intent);
    }

    public RemoteViews getRemoteViews() {
        return remoteViews_;
    }

    private void setOnClickIntent(int resId, PendingIntent intent) {
        remoteViews_.setOnClickPendingIntent(resId, intent);
    }

    private RemoteViews createView(int layoutResId) {
        return new RemoteViews(context_.getPackageName(), layoutResId);
    }

    private void setVisibility(int resId, int visibility) {
        remoteViews_.setViewVisibility(resId, visibility);
    }

    private void setText(int resId, String text) {
        remoteViews_.setTextViewText(resId, text);
    }

    private final Context context_;
    private final RemoteViews remoteViews_;
}
