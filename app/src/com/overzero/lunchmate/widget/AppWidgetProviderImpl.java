package com.overzero.lunchmate.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.overzero.lunchmate.activity.MainActivity;
import com.overzero.lunchmate.R;
import com.overzero.lunchmate.record.Record;
import com.overzero.lunchmate.record.RecordsLocalStorage;
import com.overzero.lunchmate.record.RecordsRemoteStorage;

import java.util.List;

public class AppWidgetProviderImpl extends AppWidgetProvider {

    @Override
    public void onReceive(Context context, Intent intent) {
        forceRecordLoad_ = intent.hasExtra(FORCE_RECORD_LOAD);
        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(final Context context) {
        super.onEnabled(context);

        Log.i(getClass().getSimpleName(), "onEnabled");

        updateWidgets(context, AppWidgetManager.getInstance(context));
    }

    @Override
    public void onUpdate(
            Context context,
            AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {

        super.onUpdate(context, appWidgetManager, appWidgetIds);

        Log.i(getClass().getSimpleName(), "onUpdate");

        updateWidgets(context, appWidgetManager);
        forceRecordLoad_ = false;
    }

    private void updateWidgets(
            final Context context, final AppWidgetManager appWidgetManager) {

        final int[] allWidgetIds = appWidgetManager.getAppWidgetIds(
                new ComponentName(context, AppWidgetProviderImpl.class));

        final RemoteViewsBuilder remoteViewsBuilder =
                new RemoteViewsBuilder(context);

        final RecordsLocalStorage recordLocalStorage =
                new RecordsLocalStorage(context);

        remoteViewsBuilder.setRecords(recordLocalStorage.readRecords());
        remoteViewsBuilder.setInProgress(false);

        remoteViewsBuilder.setOnListClickIntent(
                PendingIntent.getBroadcast(
                        context,
                        0,
                        new Intent(context, AppWidgetProviderImpl.class)
                                .setAction(
                                        AppWidgetManager.ACTION_APPWIDGET_UPDATE)
                                .putExtra(
                                        AppWidgetManager.EXTRA_APPWIDGET_IDS,
                                        allWidgetIds)
                                .putExtra(FORCE_RECORD_LOAD, true),
                        PendingIntent.FLAG_UPDATE_CURRENT));

        remoteViewsBuilder.setOnEditClickIntent(
                PendingIntent.getActivity(
                        context,
                        0,
                        new Intent(context, MainActivity.class),
                        0));

        if (forceRecordLoad_ || recordLocalStorage.isOutOfDate()) {
            remoteViewsBuilder.setInProgress(true);

            RecordsRemoteStorage.readRecords(
                    context.getString(R.string.lm_spreadsheet_key),
                    new RecordsRemoteStorage.ReadListener() {
                        @Override
                        public void onRecordsRead(List<Record> records) {
                            recordLocalStorage.writeRecords(records);

                            remoteViewsBuilder.setRecords(records);
                            remoteViewsBuilder.setInProgress(false);

                            for (int widgetId : allWidgetIds) {
                                appWidgetManager.updateAppWidget(
                                        widgetId,
                                        remoteViewsBuilder.getRemoteViews());
                            }
                        }
                    });
        }

        for (int widgetId : allWidgetIds) {
            appWidgetManager.updateAppWidget(
                    widgetId,
                    remoteViewsBuilder.getRemoteViews());
        }
    }

    private static final String FORCE_RECORD_LOAD = "forceRecordLoad";

    private boolean forceRecordLoad_ = false;
}
