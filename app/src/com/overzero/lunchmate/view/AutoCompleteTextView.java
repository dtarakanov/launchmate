package com.overzero.lunchmate.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class AutoCompleteTextView extends android.widget.AutoCompleteTextView {

    public interface OnBackPressedListener {
        void onBackPressed(AutoCompleteTextView editText);
    }

    public AutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoCompleteTextView(Context context) {
        super(context);
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        backPressedListener_ = listener;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_UP) {
            if (backPressedListener_ != null) {
                backPressedListener_.onBackPressed(this);
                return true;
            }
        }
        return super.onKeyPreIme(keyCode, event);
    }

    private OnBackPressedListener backPressedListener_;
}
