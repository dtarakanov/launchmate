package com.overzero.lunchmate.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.overzero.lunchmate.R;
import com.overzero.lunchmate.record.Record;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class RecordView extends FrameLayout {

    public RecordView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.lm_record, this);
        ButterKnife.inject(this);
    }

    public void setRecord(Record record) {
        personName_.setText(record.getPersonName());
        personName_.setEnabled(record.getType() != Record.Type.TIPS);
        balance_.setText(String.format("%.2f", record.getBalance()));
        balance_.setEnabled(record.getType() != Record.Type.TIPS);
    }

    @InjectView(R.id.lm_recod_person_name)
    protected TextView personName_;

    @InjectView(R.id.lm_record_balance)
    protected TextView balance_;
}
