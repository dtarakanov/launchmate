package com.overzero.lunchmate.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.overzero.lunchmate.record.Record;

import java.util.List;

public class RecordListView extends ListView {
    public RecordListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setRecords(List<Record> records) {
        setAdapter(new Adapter_(getContext(), records));
    }

    public Record getItem(int position) {
        return ((Adapter_) getAdapter()).getItem(position);
    }

    private class Adapter_ extends ArrayAdapter<Record> {
        public Adapter_(Context context, List<Record> records) {
            super(context, 0, records);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RecordView recordView = (RecordView) convertView;
            if (recordView == null) {
                recordView = new RecordView(getContext(), null);
            }
            recordView.setRecord(getItem(position));
            return recordView;
        }
    }
}
