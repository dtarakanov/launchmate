package com.overzero.lunchmate.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.overzero.lunchmate.R;
import com.overzero.lunchmate.record.Record;
import com.overzero.lunchmate.util.FormatUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class BalanceInputView extends FrameLayout {

    public interface Listener {
        void onChange(double newBalance);
        void onRemove();
    }

    public BalanceInputView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.lm_balance_input, this);

        ButterKnife.inject(this);

        setRemoveEnabled(false);
    }

    public void setRecord(Record record) {
        if (record != null && record.getBalance() != 0) {
            editText_.setText(FormatUtils.formatBalance(record.getBalance()));
            editText_.selectAll();
        }
    }

    public void setRemoveEnabled(boolean enabled) {
        removeButton_.setVisibility(enabled ? VISIBLE : GONE);
    }

    public void setListener(Listener listener) {
        listener_ = listener;
    }

    @OnClick(R.id.lm_balance_input_confirm_button)
    protected void onDone() {
        String text = editText_.getText().toString();
        double balanceChange = 0;
        if (!text.isEmpty()) {
            try {
                balanceChange = Double.valueOf(text);
            } catch (Exception ignored) {
            }
        }
        if (listener_ != null) {
            listener_.onChange(balanceChange);
        }
    }

    @OnClick(R.id.lm_balance_input_remove_button)
    protected void onRemove() {
        if (listener_ != null) {
            listener_.onRemove();
        }
    }

    @InjectView(R.id.lm_balance_input_edit)
    protected AutoCompleteTextView editText_;

    @InjectView(R.id.lm_balance_input_remove_button)
    protected View removeButton_;

    private Listener listener_;
}
