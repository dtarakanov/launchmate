package com.overzero.lunchmate.record;

import java.util.Comparator;

public class Record {
    public enum Type {
        UNDEF,
        CONSUMER,
        PAYER,
        TIPS
    }

    public Record(String personName, double balance, Type type) {
        personName_ = personName;
        balance_ = balance;
        type_ = type;
    }

    public Record(double balance) {
        this(null, balance, Type.UNDEF);
    }

    public String getPersonName() {
        return personName_;
    }

    public double getBalance() {
        return balance_;
    }

    public Type getType() {
        return type_;
    }

    public static final Comparator<Record> NAME_ASC = new Comparator<Record>() {
        @Override
        public int compare(Record lhs, Record rhs) {
            return lhs.getPersonName().compareTo(rhs.getPersonName());
        }
    };

    public static final Comparator<Record> BALANCE_DESC = new Comparator<Record>() {
        @Override
        public int compare(Record lhs, Record rhs) {
            return (int) Math.round(lhs.getBalance() - rhs.getBalance());
        }
    };

    private final String personName_;
    private final double balance_;
    private final Type type_;
}
