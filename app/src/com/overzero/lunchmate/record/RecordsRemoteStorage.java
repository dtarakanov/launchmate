package com.overzero.lunchmate.record;

import android.os.AsyncTask;

import com.overzero.lunchmate.gdata.Spreadsheet;

import java.util.List;

public class RecordsRemoteStorage {
    public interface ReadListener {
        void onRecordsRead(List<Record> records);
    }

    public interface WriteListener {
        void onRecordsWritten();
    }

    public static void readRecords(
            final String spreadsheetKey, final ReadListener listener) {

        new AsyncTask<Void, Void, List<Record>>() {
            @Override
            protected List<Record> doInBackground(Void... none) {
                try {
                    return new Spreadsheet(spreadsheetKey).loadRecords();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            public void onPostExecute(List<Record> records) {
                listener.onRecordsRead(records);
            }

        }.execute();
    }

    public static void writeRecords(
            final String spreadsheetKey,
            final List<Record> records,
            final WriteListener listener) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... none) {
                try {
                    new Spreadsheet(spreadsheetKey).pushRecords(records);
                } catch (Exception ignored) {
                }
                return null;
            }

            @Override
            public void onPostExecute(Void ignored) {
                listener.onRecordsWritten();
            }

        }.execute();
    }
}
