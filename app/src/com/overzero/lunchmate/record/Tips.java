package com.overzero.lunchmate.record;

import java.util.ArrayList;

public class Tips {

    public void reassign(Transaction transaction) {
        final ArrayList<Record> records =
                new ArrayList<Record>(transaction.getRecords());
        for (Record record : records) {
            if (record.getType() == Record.Type.TIPS) {
                transaction.removeRecord(record);
            }
        }

        final ArrayList<Record> tips =
                new ArrayList<Record>(transaction.getRecords());

        final double tipsSum = transaction.getTotalBalance();
        if (tipsSum <= 0) {
            return;
        }

        double totalCharge = 0;
        for (Record record : transaction.getRecords()) {
            if (record.getType() == Record.Type.CONSUMER &&
                    !Double.isNaN(record.getBalance())) {
                totalCharge += record.getBalance();
            }
        }

        if (totalCharge != 0) {
            for (Record record : transaction.getRecords()) {
                if (record.getType() == Record.Type.CONSUMER &&
                        !Double.isNaN(record.getBalance())) {
                    transaction.addTips(
                            record.getPersonName(),
                            record.getBalance() * tipsSum / totalCharge);
                }
            }
        }
    }
}
