package com.overzero.lunchmate.record;

import android.content.Context;
import android.content.SharedPreferences;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class RecordsLocalStorage {

    public RecordsLocalStorage(Context context) {
        commonPreferences_ =
                context.getSharedPreferences("common", Context.MODE_PRIVATE);
        recordPreferences_ =
                context.getSharedPreferences("records", Context.MODE_PRIVATE);
    }

    public boolean isOutOfDate() {
        final String updateTimeString = commonPreferences_.getString(
                UPDATE_TIME, null);

        if (updateTimeString == null) {
            return true;
        }

        final DateTime updateTime = DateTime.parse(updateTimeString);
        final DateTime outOfDateTime = DateTime.now().minusMinutes(5);

        return updateTime.isBefore(outOfDateTime);
    }

    public void writeRecords(List<Record> records) {
        final SharedPreferences.Editor editor = recordPreferences_.edit();
        editor.clear();

        for (Record record : records) {
            editor.putFloat(
                    record.getPersonName(), (float) record.getBalance());
        }

        editor.apply();

        commonPreferences_.edit()
                .putString(UPDATE_TIME, DateTime.now().toString())
                .apply();
    }

    public List<Record> readRecords() {
        final Map<String, ?> preferences = recordPreferences_.getAll();

        final ArrayList<Record> records =
                new ArrayList<Record>(preferences.size());

        for (Map.Entry<String, ?> preference : preferences.entrySet()) {
            records.add(
                    new Record(
                            preference.getKey(),
                            (Float) preference.getValue(),
                            Record.Type.UNDEF));
        }

        Collections.sort(records, Record.NAME_ASC);
        Collections.sort(records, Record.BALANCE_DESC);
        return records;
    }

    public static final String UPDATE_TIME = "updateTime";

    private final SharedPreferences commonPreferences_;
    private final SharedPreferences recordPreferences_;
}
