package com.overzero.lunchmate.record;

import com.overzero.lunchmate.record.Record;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Transaction {
    public Transaction(List<Record> total, List<Record> current) {
        freeConsumers_ = new ArrayList<String>(total.size());
        for (Record record : total) {
            freeConsumers_.add(record.getPersonName());
        }

        freePayers_ = new ArrayList<String>(freeConsumers_);

        records_ = new ArrayList<Record>(freeConsumers_.size() * 2);

        for (Record record : current) {
            if (record.getType() == Record.Type.CONSUMER) {
                addConsumer(record.getPersonName(), record.getBalance());
            } else {
                addPayer(record.getPersonName(), record.getBalance());
            }
        }
    }

    public List<String> getFreeConsumers() {
        return freeConsumers_;
    }

    public List<String> getFreePayers() {
        return freePayers_;
    }

    public void addConsumer(String personName, double balanceChange) {
        if (balanceChange < 0) {
            throw new RuntimeException("Negative balance change: " + personName);
        }

        if (!freeConsumers_.contains(personName)) {
            throw new RuntimeException("Already a consumer: " + personName);
        }

        freeConsumers_.remove(personName);
        records_.add(new Record(personName, -balanceChange, Record.Type.CONSUMER));
    }

    public void addPayer(String personName, double balanceChange) {
        if (balanceChange < 0) {
            throw new RuntimeException("Negative balance change: " + personName);
        }

        if (!freePayers_.contains(personName)) {
            throw new RuntimeException("Already a payer: " + personName);
        }

        freePayers_.remove(personName);
        records_.add(new Record(personName, balanceChange, Record.Type.PAYER));
    }

    public void addTips(String personName, double balanceChange) {
        records_.add(new Record(personName, balanceChange, Record.Type.TIPS));
    }

    public void removeConsumer(String personName) {
        for (Record record : records_) {
            if (record.getType() == Record.Type.CONSUMER
                    && record.getPersonName().equals(personName)) {
                removeRecord(record);
                break;
            }
        }
    }

    public void removePayer(String personName) {
        for (Record record : records_) {
            if (record.getType() == Record.Type.PAYER
                    && record.getPersonName().equals(personName)) {
                removeRecord(record);
                break;
            }
        }
    }

    public void removeRecord(Record record) {
        switch (record.getType()) {
        case UNDEF:
        case TIPS:
            return;

        case CONSUMER:
            freeConsumers_.add(record.getPersonName());
            break;

        case PAYER:
            freePayers_.add(record.getPersonName());
            break;
        }
        records_.remove(record);
    }

    public void updateConsumer(String personName, double balanceChange) {
        removeConsumer(personName);
        addConsumer(personName, balanceChange);
    }

    public void updatePayer(String personName, double balanceChange) {
        removePayer(personName);
        addPayer(personName, balanceChange);
    }

    public List<Record> getRecords() {
        return Collections.unmodifiableList(records_);
    }

    public double getTotalBalance() {
        double totalBalance = 0;
        for (Record record : records_) {
            totalBalance += record.getBalance();
        }
        return totalBalance;
    }

    public boolean hasUnassignedBalances() {
        for (Record record : records_) {
            if (record.getBalance() == 0) {
                return true;
            }
        }
        return false;
    }

    private final ArrayList<String> freeConsumers_;
    private final ArrayList<String> freePayers_;
    private final ArrayList<Record> records_;
}
