package com.overzero.lunchmate.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.overzero.lunchmate.R;
import com.overzero.lunchmate.gdata.Spreadsheet;
import com.overzero.lunchmate.record.Record;
import com.overzero.lunchmate.record.RecordsLocalStorage;
import com.overzero.lunchmate.record.RecordsRemoteStorage;
import com.overzero.lunchmate.update.AppUpdaterAsync;
import com.overzero.lunchmate.util.Logger;
import com.overzero.lunchmate.view.RecordListView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lm_app_main);
        ButterKnife.inject(this);

        progressOverlay_.setVisibility(View.GONE);

        recordLocalStorage_ = new RecordsLocalStorage(this);

        AppUpdaterAsync.checkForUpdates(
                getString(R.string.lm_spreadsheet_key),
                new AppUpdaterAsync.Listener() {
                    @Override
                    public void onAppUpdatesChecked(String appUrl) {
                        Logger.get(MainActivity.class).info("App URL: %s", appUrl);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (recordLocalStorage_.isOutOfDate()) {
            refreshList();
        } else {
            personsList_.setRecords(recordLocalStorage_.readRecords());
        }
    }

    @OnClick(R.id.lm_main_refresh_button)
    protected void refreshList() {
        progressOverlay_.setVisibility(View.VISIBLE);

        RecordsRemoteStorage.readRecords(
                getString(R.string.lm_spreadsheet_key),
                new RecordsRemoteStorage.ReadListener() {
                    @Override
                    public void onRecordsRead(List<Record> records) {
                        progressOverlay_.setVisibility(View.GONE);

                        if (records == null) {
                            showLoadError();
                            return;
                        }

                        personsList_.setRecords(records);
                        recordLocalStorage_.writeRecords(records);
                    }
                });
    }

    @OnClick(R.id.lm_main_add_transaction_button)
    protected void addLunch() {
        Intent intent = new Intent(this, AddTransactionActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.lm_main_go_to_google_drive_button)
    protected void goToGoogleDrive() {
        final Spreadsheet infoLoader = new Spreadsheet(
                getString(R.string.lm_spreadsheet_key));

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(infoLoader.getWorksheetUri());
        startActivity(intent);
    }

    private void showLoadError() {
        final Toast toast = Toast.makeText(
                this, R.string.lm_load_error, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @InjectView(R.id.lm_main_persons_list)
    protected RecordListView personsList_;

    @InjectView(R.id.lm_main_progress_overlay)
    protected View progressOverlay_;

    private RecordsLocalStorage recordLocalStorage_;
}
