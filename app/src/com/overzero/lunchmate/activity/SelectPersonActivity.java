package com.overzero.lunchmate.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.overzero.lunchmate.R;

public class SelectPersonActivity extends Activity {

    public static void start(String title, List<String> personList,
            int resultCode, Activity callerActivity) {

        Intent intent = new Intent(callerActivity, SelectPersonActivity.class);
        intent.putExtra(Keys.ACTIVITY_TITLE, title);
        intent.putExtra(Keys.PERSON_LIST,
                personList.toArray(new String[personList.size()]));
        callerActivity.startActivityForResult(intent, resultCode);
    }

    public static String getPersonName(Intent intent) {
        return intent.getStringExtra(Keys.PERSON_NAME);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lm_app_persons);
        setTitle(getIntent().getStringExtra(Keys.ACTIVITY_TITLE));

        adapter_ = new ArrayAdapter<String>(this,
                R.layout.lm_record,
                R.id.lm_recod_person_name,
                getIntent().getStringArrayExtra(Keys.PERSON_LIST));

        ListView personList = (ListView) findViewById(R.id.person_list);
        personList.setAdapter(adapter_);
        personList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {

                onItemClicked(position);
            }
        });
    }

    private void onItemClicked(int position) {
        Intent intent = new Intent();
        intent.putExtra(Keys.PERSON_NAME, adapter_.getItem(position));
        setResult(RESULT_OK, intent);
        finish();
    }

    private ArrayAdapter<String> adapter_;
}
