package com.overzero.lunchmate.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.overzero.lunchmate.R;
import com.overzero.lunchmate.record.Record;
import com.overzero.lunchmate.record.RecordsLocalStorage;
import com.overzero.lunchmate.record.RecordsRemoteStorage;
import com.overzero.lunchmate.record.Transaction;
import com.overzero.lunchmate.view.RecordListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class AddTransactionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lm_transaction);

        ButterKnife.inject(this);

        final RecordsLocalStorage storage = new RecordsLocalStorage(this);

        transaction_ = new Transaction(
                storage.readRecords(),
                restoreRecords(savedInstanceState));

        addEaterButton_ = findViewById(R.id.lunch_add_eater_button);
        addEaterButton_.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectEaterName();
                    }
                });

        addPayerButton_ = findViewById(R.id.lunch_add_payer_button);
        addPayerButton_.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectPayerName();
                    }
                });

        recordsList_.setOnItemClickListener(
                new ListView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id) {
                        onRecordClick(position);
                    }
                });

        updateViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        final ArrayList<String> recordStrings =
                new ArrayList<String>(transaction_.getRecords().size());
        for (Record record : transaction_.getRecords()) {
            // TODO: Use not string list?
            recordStrings.add(record.getPersonName());
            recordStrings.add(String.valueOf(record.getBalance()));
        }
        state.putStringArrayList(Keys.LUNCH_RECORDS, recordStrings);
    }

    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent intent) {
/*
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
        case SELECT_EATER:
            onEaterSelected(SelectPersonActivity.getPersonName(intent));
            break;

        case ENTER_CHARGE:
            addEaterRecord(
                    BalanceInputActivity.getPersonName(intent),
                    BalanceInputActivity.getBalanceChange(intent));
            break;

        case SELECT_PAYER:
            enterPayment(SelectPersonActivity.getPersonName(intent));
            break;

        case ENTER_PAYMENT:
            addPayerRecord(
                    BalanceInputActivity.getPersonName(intent),
                    BalanceInputActivity.getBalanceChange(intent));
            break;

        case EDIT_CHARGE:
            if (Double.isNaN(BalanceInputActivity.getBalanceChange(intent))) {
                removeEaterRecord(BalanceInputActivity.getPersonName(intent));
            } else {
                updateEaterRecord(
                        BalanceInputActivity.getPersonName(intent),
                        BalanceInputActivity.getBalanceChange(intent));
            }
            break;

        case EDIT_PAYMENT:
            if (Double.isNaN(BalanceInputActivity.getBalanceChange(intent))) {
                removePayerRecord(BalanceInputActivity.getPersonName(intent));
            } else {
                updatePayerRecord(
                        BalanceInputActivity.getPersonName(intent),
                        BalanceInputActivity.getBalanceChange(intent));
            }
            break;
        }*/
    }

    @Override
    public void onBackPressed() {
        if (transaction_.getRecords().isEmpty()) {
            super.onBackPressed();
        } else {
            showUnsavedAlert();
        }
    }

    @OnItemClick(R.id.lunch_records_list)
    protected void removeRecord(int position) {
        final Record record = recordsList_.getItem(position);
        transaction_.removeRecord(record);
        updateViews();
    }

    @OnItemClick(R.id.lunch_records_list)
    protected void onRecordClick(int position) {
        final Record record = recordsList_.getItem(position);
        editRecord(record);
    }

    @InjectView(R.id.lunch_records_list)
    protected RecordListView recordsList_;

    @InjectView(R.id.lunch_add_eater_button)
    protected View addEaterButton_;

    @InjectView(R.id.lunch_add_payer_button)
    protected View addPayerButton_;

    @InjectView(R.id.lunch_commit_button)
    protected View submitButton_;

    private void showUnsavedAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.lm_exit);
        builder.setMessage(R.string.lm_exit_unsaved_warning);
        builder.setCancelable(true);
        builder.setPositiveButton(
                R.string.lm_exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            finish();
                        }
                    }
                });
        builder.create().show();
    }

    private List<Record> restoreRecords(Bundle savedInstanceState) {
        final List<Record> records = new ArrayList<Record>();
        if (savedInstanceState != null) {
            final List<String> recordStrings =
                    savedInstanceState.getStringArrayList(Keys.LUNCH_RECORDS);
            if (recordStrings != null) {
                for (int i = 0; i < recordStrings.size(); i += 2) {
                    final Record record = new Record(
                            recordStrings.get(i),
                            Double.valueOf(recordStrings.get(i + 1)),
                            Record.Type.CONSUMER);
                    records.add(record);
                }
            }
        }
        return records;
    }


    private void editRecord(Record record) {
        /*BalanceInputActivity.start(
                getString(
                        record.getType() == Type.CONSUMER
                                ? R.string.lm_person_ate_format
                                : R.string.lm_person_paid_format,
                        record.getPersonName()),
                record.getPersonName(),
                Math.abs(record.getBalance()),
                true,
                record.getType() == Type.CONSUMER ? EDIT_CHARGE : EDIT_PAYMENT,
                this);*/
    }

    private void selectEaterName() {
        SelectPersonActivity.start(
                getString(R.string.lm_consumer_addition),
                transaction_.getFreeConsumers(),
                SELECT_EATER,
                this);
    }

    private void onEaterSelected(String personName) {
        /*BalanceInputActivity.start(
                getString(R.string.lm_person_ate_format, personName),
                personName,
                Double.NaN,
                false,
                ENTER_CHARGE,
                this);*/
    }

    private void addEaterRecord(String personName, double balanceChange) {
        transaction_.addConsumer(personName, balanceChange);
        updateViews();
        focusLastRecord();
    }

    @OnClick(R.id.lunch_add_payer_button)
    protected void selectPayerName() {
        SelectPersonActivity.start(
                getString(R.string.lm_payer_addition),
                transaction_.getFreePayers(),
                SELECT_PAYER,
                this);
    }

    private void enterPayment(String personName) {
        /*BalanceInputActivity.start(
                getString(R.string.lm_person_paid_format, personName),
                personName,
                Double.NaN,
                false,
                ENTER_PAYMENT,
                this);*/
    }

    private void addPayerRecord(String personName, double balanceChange) {
        transaction_.addPayer(personName, balanceChange);
        updateViews();
        focusLastRecord();
    }

    private void updateEaterRecord(String personName, Double balanceChange) {
        transaction_.updateConsumer(personName, balanceChange);
        updateViews();
    }

    private void updatePayerRecord(String personName, Double balanceChange) {
        transaction_.updatePayer(personName, balanceChange);
        updateViews();
    }

    private void removeEaterRecord(String personName) {
        transaction_.removeConsumer(personName);
        updateViews();
    }

    private void removePayerRecord(String personName) {
        transaction_.removePayer(personName);
        updateViews();
    }

    @OnClick(R.id.lunch_commit_button)
    protected void submit() {
        if (transaction_.getTotalBalance() == 0) {
            pushRecords();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.lm_transaction_commitment);
        builder.setMessage(
                String.format(
                        getString(R.string.lm_distribute_tips_question_format),
                        transaction_.getTotalBalance()));
        builder.setCancelable(true);
        builder.setPositiveButton(
                R.string.lm_distribute, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            pushRecords();
                        }
                    }
                });
        builder.create().show();
    }

    private void pushRecords() {
        RecordsRemoteStorage.writeRecords(
                getString(R.string.lm_spreadsheet_key),
                transaction_.getRecords(),
                new RecordsRemoteStorage.WriteListener() {
                    @Override
                    public void onRecordsWritten() {
                        finish();
                    }
                });
    }

    private void focusLastRecord() {
        recordsList_.post(
                new Runnable() {
                    @Override
                    public void run() {
                        recordsList_.setSelection(
                                transaction_.getRecords().size() - 1);
                    }
                });
    }

    private void updateViews() {
        final ArrayList<Record> records =
                new ArrayList<Record>(transaction_.getRecords());
        recordsList_.setRecords(records);

        addEaterButton_.setEnabled(!transaction_.getFreeConsumers().isEmpty());
        addPayerButton_.setEnabled(!transaction_.getFreePayers().isEmpty());
        submitButton_.setEnabled(
                !transaction_.getRecords().isEmpty() &&
                        !transaction_.hasUnassignedBalances() &&
                        transaction_.getTotalBalance() >= 0);
    }

    private static final int SELECT_EATER = 0;
    private static final int ENTER_CHARGE = 1;
    private static final int SELECT_PAYER = 2;
    private static final int ENTER_PAYMENT = 3;
    private static final int EDIT_CHARGE = 4;
    private static final int EDIT_PAYMENT = 5;

    private Transaction transaction_;
}
