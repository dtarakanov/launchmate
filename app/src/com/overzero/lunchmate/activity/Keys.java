package com.overzero.lunchmate.activity;

public class Keys {

    public static final String ACTIVITY_TITLE = "activityTitle";
    public static final String PERSON_LIST = "personList";
    public static final String PERSON_NAME = "personName";
    public static final String BALANCE_CHANGE = "balanceChange";
    public static final String LUNCH_RECORDS = "lunchRecords";
    public static final String IS_REMOVABLE = "isRemovable";
}
