package com.overzero.lunchmate.update;

import android.os.AsyncTask;

public class AppUpdaterAsync {

    public interface Listener {
        void onAppUpdatesChecked(String appUrl);
    }

    public static void checkForUpdates(
            final String spreadsheetKey, final Listener listener) {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... none) {
                try {
                    return new AppUpdater(spreadsheetKey).getAppUrl();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            public void onPostExecute(String appUrl) {
                listener.onAppUpdatesChecked(appUrl);
            }

        }.execute();
    }

}
