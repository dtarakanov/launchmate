package com.overzero.lunchmate.update;

import com.google.gdata.util.ServiceException;
import com.overzero.lunchmate.gdata.Spreadsheet;

import java.io.IOException;

public class AppUpdater {

    public AppUpdater(String spreadsheetKey) {
        spreadsheetKey_ = spreadsheetKey;
    }

    public String getAppUrl() throws IOException, ServiceException {
        return new Spreadsheet(spreadsheetKey_).getAppUrl();
    }

    private final String spreadsheetKey_;
}
