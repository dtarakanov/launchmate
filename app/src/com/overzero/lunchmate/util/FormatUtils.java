package com.overzero.lunchmate.util;

import com.overzero.lunchmate.record.Record;

import java.util.List;

public class FormatUtils {
    public static String format(List<Record> info, String rowFormat) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < info.size(); ++i) {
            builder.append(
                    String.format(rowFormat, i + 1,
                            info.get(i).getPersonName(),
                            info.get(i).getBalance()));
            builder.append('\n');
        }
        return builder.toString();
    }

    public static String formatBalance(double balance) {
        return String.format("%.2f", balance);
    }
}
