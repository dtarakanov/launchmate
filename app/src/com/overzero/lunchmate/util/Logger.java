package com.overzero.lunchmate.util;

import android.util.Log;

public class Logger {
    public static Logger get(Class<?> klass) {
        return get(klass.getSimpleName());
    }

    public static Logger get(String tag) {
        return new Logger(tag);
    }

    public void error(String format, Object... args) {
        if (Log.isLoggable(tag_, Log.ERROR)) {
            Log.e(tag_, String.format(format, args));
        }
    }

    public void warn(String format, Object... args) {
        if (Log.isLoggable(tag_, Log.WARN)) {
            Log.w(tag_, String.format(format, args));
        }
    }

    public void info(String format, Object... args) {
        if (Log.isLoggable(tag_, Log.INFO)) {
            Log.i(tag_, String.format(format, args));
        }
    }

    public void debug(String format, Object... args) {
        if (Log.isLoggable(tag_, Log.DEBUG)) {
            Log.d(tag_, String.format(format, args));
        }
    }

    private Logger(String tag) {
        tag_ = tag;
    }

    private final String tag_;
}
