package com.overzero.lunchmate.util;

import android.content.Context;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

import com.overzero.lunchmate.R;

public class ToastUtils {
    public static void showToast(Context context, @StringRes int textResId) {
        final Toast toast = Toast.makeText(
                context,
                R.string.lm_widget_data_load_error_message,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
