package com.overzero.lunchmate.gdata;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.net.Uri;

import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.client.spreadsheet.WorksheetQuery;
import com.google.gdata.data.Link;
import com.google.gdata.data.batch.BatchOperationType;
import com.google.gdata.data.batch.BatchStatus;
import com.google.gdata.data.batch.BatchUtils;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;
import com.overzero.lunchmate.util.Logger;
import com.overzero.lunchmate.record.Record;

public class Spreadsheet {
    public Spreadsheet(String spreadsheetKey) {
        spreadsheetKey_ = spreadsheetKey;

        service_ = new SpreadsheetService("LunchMate");
        // service_.setConnectTimeout(4000);
        // service_.setReadTimeout(4000);
    }

    public List<Worksheet> getWorksheets() throws IOException,
            ServiceException {
        final URL worksheetFeedUrl = FeedURLFactory.getDefault()
                .getWorksheetFeedUrl(
                        spreadsheetKey_, VISIBILITY, PROJECTION);

        final WorksheetQuery worksheetQuery =
                new WorksheetQuery(worksheetFeedUrl);
        final WorksheetFeed worksheetFeed =
                service_.query(worksheetQuery, WorksheetFeed.class);

        final ArrayList<Worksheet> worksheets =
                new ArrayList<Worksheet>(worksheetFeed.getEntries().size());
        for (WorksheetEntry worksheetEntry : worksheetFeed.getEntries()) {
            worksheets.add(new Worksheet(service_, worksheetEntry));
        }
        return worksheets;
    }

    public Uri getWorksheetUri() {
        String baseUrl = FeedURLFactory.getDefault().getBaseUrl().toString();
        return Uri.parse(baseUrl)
                .buildUpon()
                .appendPath("spreadsheet")
                .appendPath("ccc")
                .appendQueryParameter("key", spreadsheetKey_)
                .build();
    }

    public String getAppUrl() throws IOException, ServiceException {
        final List<Worksheet> worksheets = getWorksheets();
        final Worksheet lastWorksheet = worksheets.get(worksheets.size() - 1);
        final Cells cells = lastWorksheet.getCells(1, 1, 2, 2);
        final Cell cell = cells.getCell(1, 2);
        return cell != null ? cell.getValue() : null;
    }

    public List<Record> loadRecords() throws IOException, ServiceException {
        final List<Record> records = new ArrayList<Record>();
        final Map<String, String> nameToColumn = new HashMap<String, String>();

        final List<Worksheet> worksheets = getWorksheets();
        final Worksheet firstWorksheet = worksheets.get(0);
        final Cells cells = firstWorksheet.getCells(
                VALUE_ROW, NAME_ROW, FIRST_COL, Worksheet.UNSPECIFIED);

        final Map<String, Record> columnToRecord =
                new HashMap<String, Record>();

        for (int col = FIRST_COL; ; col += 2) {
            final Cell nameCell = cells.getCell(NAME_ROW, col);
            final Cell valueCell = cells.getCell(VALUE_ROW, col);
            if (nameCell == null || valueCell == null) {
                break;
            }
            records.add(new Record(
                    nameCell.getValue(),
                    Double.valueOf(valueCell.getValue().replace(',', '.')),
                    Record.Type.UNDEF));
        }

        Collections.sort(records, Record.NAME_ASC);
        Collections.sort(records, Record.BALANCE_DESC);
        return records;
    }

    public void pushRecords(List<Record> records) throws IOException,
            ServiceException {
        final SpreadsheetService service = new SpreadsheetService("LunchMate");
        URL cellsFeedUrl = FeedURLFactory.getDefault().getCellFeedUrl(
                spreadsheetKey_, RECORDS_WORKSHEET_KEY, VISIBILITY, PROJECTION);

        CellFeed batchRequest = new CellFeed();
        CellEntry batchEntry = new CellEntry(
                12,
                4,
                String.valueOf(records.get(0).getBalance()));
        //batchEntry.setId(String.format("%s/%s", cellFeedUrl.toString(),
        // cellId.idString));
        //BatchUtils.setBatchId(batchEntry, cellId.idString);
        BatchUtils.setBatchOperationType(batchEntry, BatchOperationType.INSERT);
        batchRequest.getEntries().add(batchEntry);

        CellFeed cellFeed = service.getFeed(cellsFeedUrl, CellFeed.class);
        CellFeed queryBatchResponse = service.batch(new URL(
                cellFeed.getLink(Link.Rel.FEED_BATCH, Link.Type.ATOM).getHref()),
                batchRequest);

        Map<String, CellEntry> cellEntryMap = new HashMap<String, CellEntry>();
        for (CellEntry entry : queryBatchResponse.getEntries()) {
            cellEntryMap.put(BatchUtils.getBatchId(entry), entry);
            LOGGER.info(
                    "batch %s {CellEntry: id=%s editLink=%s inputValue=%s}",
                    BatchUtils.getBatchId(entry),
                    entry.getId(),
                    entry.getEditLink().getHref(),
                    entry.getCell().getInputValue());
        }

        // Submit the update
        Link batchLink = cellFeed.getLink(Link.Rel.FEED_BATCH, Link.Type.ATOM);
        service.setHeader("If-Match", "*");
        CellFeed batchResponse =
                service.batch(new URL(batchLink.getHref()), batchRequest);
        service.setHeader("If-Match", null);

        // Check the results
        boolean isSuccess = true;
        for (CellEntry entry : batchResponse.getEntries()) {
            String batchId = BatchUtils.getBatchId(entry);
            if (!BatchUtils.isSuccess(entry)) {
                isSuccess = false;
                BatchStatus status = BatchUtils.getBatchStatus(entry);
                LOGGER.warn(
                        "%s failed (%s) %s",
                        batchId,
                        status.getReason(),
                        status.getContent());
            }
        }
    }

    private static final Logger LOGGER = Logger.get(Spreadsheet.class);
    private static final String RECORDS_WORKSHEET_KEY = "od6";
    private static final String VISIBILITY = "public";
    private static final String PROJECTION = "basic";

    private static final int VALUE_ROW = 2;
    private static final int NAME_ROW = 3;
    private static final int FIRST_COL = 4;

    private final String spreadsheetKey_;
    private final SpreadsheetService service_;
}
