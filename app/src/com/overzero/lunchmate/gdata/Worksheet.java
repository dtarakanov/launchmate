package com.overzero.lunchmate.gdata;

import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;
import com.overzero.lunchmate.util.Logger;

import java.io.IOException;
import java.net.URL;

class Worksheet {

    public static int UNSPECIFIED = -1;

    Worksheet(SpreadsheetService service, WorksheetEntry worksheetEntry) {
        service_ = service;
        worksheetEntry_ = worksheetEntry;
    }

    public Cells getCells(int minRow, int maxRow, int minCol, int maxCol) throws
            IOException, ServiceException {
        final URL cellsFeedUrl = worksheetEntry_.getCellFeedUrl();

        LOGGER.info("Fetching cells from URL: %s", cellsFeedUrl);

        final CellQuery query = new CellQuery(cellsFeedUrl);
        if (minRow != UNSPECIFIED) {
            query.setMinimumRow(minRow);
        }
        if (maxRow != UNSPECIFIED) {
            query.setMaximumRow(maxRow);
        }
        if (minCol != UNSPECIFIED) {
            query.setMinimumCol(minCol);
        }
        if (maxCol != UNSPECIFIED) {
            query.setMaximumCol(maxCol);
        }

        final CellFeed cellFeed = service_.query(query, CellFeed.class);
        return new Cells(service_, cellsFeedUrl, cellFeed.getEntries());
    }

    private static final Logger LOGGER = Logger.get(Worksheet.class);

    private final SpreadsheetService service_;
    private final WorksheetEntry worksheetEntry_;
}
