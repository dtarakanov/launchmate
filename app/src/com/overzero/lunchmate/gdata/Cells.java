package com.overzero.lunchmate.gdata;

import android.support.v4.util.SparseArrayCompat;
import android.util.SparseArray;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;

import java.net.URL;
import java.util.List;

class Cells {

    Cells(SpreadsheetService service, URL cellFeedUrl, List<CellEntry> entries) {
        cells_ = new SparseArrayCompat<SparseArray<Cell>>();

        for (CellEntry entry : entries) {
            final String address = entry.getTitle().getPlainText();

            for (int i = 0; i < address.length(); ++i) {
                if (Character.isDigit(address.charAt(i))) {
                    int col = colLettersToNum(address.substring(0, i));
                    int row = Integer.valueOf(address.substring(i));

                    getRow(row).put(col, new Cell(service, entry, cellFeedUrl));
                }
            }
        }
    }

    private int colLettersToNum(String letters) {
        switch (letters.length()) {
        case 1:
            return colLetterToNum(letters, 0);

        case 2:
            return 26 * colLetterToNum(letters, 0) + colLetterToNum(letters, 1);

        default:
            return -1;
        }
    }

    private int colLetterToNum(String letters, int index) {
        return Character.codePointAt(letters, index) -
                Character.codePointAt("A", 0) + 1;
    }

    public Cell getCell(int row, int col) {
        return getRow(row).get(col);
    }

    private SparseArray<Cell> getRow(int row) {
        if (cells_.get(row) == null) {
            cells_.put(row, new SparseArray<Cell>());
        }
        return cells_.get(row);
    }

    private final SparseArrayCompat<SparseArray<Cell>> cells_;
}
