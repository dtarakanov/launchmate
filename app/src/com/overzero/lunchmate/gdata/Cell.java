package com.overzero.lunchmate.gdata;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.util.ServiceException;

import java.io.IOException;
import java.net.URL;

class Cell {

    Cell(SpreadsheetService service, CellEntry cellEntry, URL cellFeedUrl) {
        service_ = service;
        cellEntry_ = cellEntry;
        cellFeedUrl_ = cellFeedUrl;
    }

    public String getValue() {
        return cellEntry_.getPlainTextContent();
    }

    public void setValue(String value) throws IOException, ServiceException {
        cellEntry_.changeInputValueLocal(value);
        service_.insert(cellFeedUrl_, cellEntry_);
    }

    private final SpreadsheetService service_;
    private final CellEntry cellEntry_;
    private final URL cellFeedUrl_;
}
